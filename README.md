
# 团队协作APP_前端产品文档



## MRD

### （一）项目介绍
***
- 小组协作式Axure项目：团队协作APP，由中山大学南方学院文学与传媒学院网络与新媒体专业学生制作。 
- 成员：刘燕鑫、刘炜豪、廖俊杰、罗嘉琪、蔡晓晨、张杰

### （二）产品构想
***
#### 1. 产品背景
互联网行业不断创新发展，面对变化莫测的市场，产品敏捷迭代，信息速率逐步提升。在这样的环境中，人们分别在不同电子产品工作的效果会显得力不从心，而在网上的协作则显得尤为重要，可对信息随时沟通、修改和完善等等。

#### 2. 产品核心目标
团队协作APP助力互联网行业提高人们的网上的协作效率，加强信息的及时性沟通，消除信息堡垒。具体为产品经理使用团队协作app撰写PRD需求文档，将需求背景、开发周期等等清晰呈现。研发部门和项目成员可随时查看、评论、协同编写等等，保证了团队工作时的信息快速同步，以最大的效率进行团队协作。

#### 3. 产品定位
团队协作APP，用户团队之间线上合作。

#### 4. 产品核心功能

- 模板库功能（亮点功能）：用户进入“模板库”页面，可新增项目，同时提供项目模板。随后再进行任务指派，规定每个成员不同的任务和截止时间；

- 在线共同编辑功能：用户进入“工作台”页面，可见“参与的项目列表”，用户可对当前项目进行内容的编辑；

- 项目管理功能：用户进入“历史创建”页面，可查看已创建项目的完成进度，同时也可以管理项目成员、任务及内容。

### （三）目标市场现状及分析
***
#### 1. 市场背景
目前市场拥有许多不同类型的团队协作APP，且不同APP各有所长，通过调查市场APP总结其中的优点和缺点，从而更好的设计本产品。

#### 2. 市场调研数据
|  访谈的对象   | 大学生群体  | 大一大二大三  | 男性>女性  |同专业的同学  | 身边的朋友  |
|  ----  | ----  | ----  | ----  | ----  | ----  |
| 抽样方法  | 问卷调查  |问卷调查  | 问卷调查  | 无结构访谈  |无结构访谈  |
| 样本规模大小  | 40份 |  40份   | 30份 | 3份  | 3份  |


- [调查问卷链接](https://www.wjx.cn/jq/52095459.aspx)

#### 3. 无结构访谈的市场调研结论
|  市场调研内容总结   | 谁是我们的目标用户 | 目标用户使用目前的协作工具之后，感受是什么  | 目标用户希望有什么类型的功能解决他们现在遇到的问题  |
|  ----  | ----  | ----  | ----  |
| 基本方向及用户典型特征  | 4个人以上的团队  |信任也困惑、焦虑、无聊 | 智能日历、高效开会、消息“串”联、静音回复、智能提醒、在线文档、云端文件库  |

#### 4. 市场问题（用户核心痛点）

- 在团队协作的过程中，用户持续在不同办公工具之间来回切换，并且产生烦躁等负面情绪。

- 在团队协作的过程中，被太多分散注意力的信息打扰，被无关消息刷屏。

- 在团队协作的过程中，很难快速搜索到和编辑想要的所有协作信息和文档。

- 在团队协作的过程中，同步更新信息的时候很容易被用户遗漏或者无视，有时需要提醒多次。（如在微信协作时，通过在群里更新信息时信息很容易被覆盖或遗漏）

#### 5. 市场机会

- 找出并结合用户团队协作时不断切换工具需要的最核心的几大功能，并把此类功能整合在我们的产品中。

- 提供智能提醒功能，自动推送任务截止时间信息，提醒成员尽快完成工作。

- 提供表情静音回复的功能，降低杂噪音及回复“收到”等信息的刷屏影响。

- 开通在线文档功能，它是富文本的形式，满足兼容多样文本查阅和编辑的需求，同时也可以让文档上传到云端文件库，并可以设置成共享的模式供成员共享。

### （四）竞品分析
***
#### 竞品①——Teambition
##### 优点：

1. 项目模板丰富。

2. 应用商店丰富。（这其实也不算个优点，高效一定是简单的，越丰富套路越多，越难选择）。

3. 功能丰富。（还是一句话，如果学习成本高，管理复杂，这也不能算优点）。

##### 缺点：

1. 部分流程操作有点混乱。

2. 部分插件功能较差含糊，建议一定要精简，企业本来就想提高效率，结果还得让企业各种纠结选择。

3. 需求的优先级竟然不在第一屏，还得拖动排序。

4. 项目管理和企业管理界面等功能内部既然没法跳转。

#### 竞品②——tapd
##### 优点：
1. 界面以及操作十分精简。

![image.png](https://images.gitee.com/uploads/images/2019/1224/164416_b4787583_1648153.png)

2. 提示十分友好，这是tapd创建完项目之后弹出的操作提示，显得非常专业

![image.png](https://images.gitee.com/uploads/images/2019/1224/164416_12d4a469_1648153.png)

3. 界面克制，项目的应用就最多放7个，多了不让放。一个项目管理如果流程太多，基本上最终死的可能性很大。而且大部分时间都会浪费在管理这些过程上，谈何敏捷。

##### 缺点：

1. 缺少目标管理，如：OKR和KPI管理，个人感觉很重要，目标管理是处项目管理之外，个人进步的一个很大驱动因素。这个很多企业应该还是exel模式吧。

2. 缺少领导视角，如：全局查看所有项目的概况和统计信息，目前只能单个项目查看。

3. 手机端没有app，只有服务号。造成没必要的二次点击和操作不便，虽然依托微信这个便利，但是有了企业微信，工作中还是建议从企业微信或者app进去。

### （五）用户分析
***
#### 1. 目标用户
主要用于团队协作处理需求的小组。

#### 2. 用户细分
- 10人左右使用，用户同学等沟通不便情况。
- 小微团队，30人左右使用，用于学校部门间协作沟通效率不佳情况。
- 中型团队，70人左右使用，用于学校团队间的沟通合作。

#### 3. 用户场景

- 用户拥有多个项目，所有项目堆在一起会变得很乱，查找起来也比较麻烦，所以为方便管理，用户河将项目进行分类，根据项目类型和管理风格的不同设置不同的分组。

- 由于承接的项目比较多，所以拥有很多个项目小团队，每个人的时间进度都不一样，要集齐大家一起开会时间不太好确定；这时使用团队日历可以查看每个成员的时间任务情况，选择恰当的时间段快速发起会议或调配资源，降低信息获取成本。

- 项目负责人需要对项目的整体进度进行把控，了解各个流程的任务进度；项目负责人把该项目生成项目报告，能透视出项目任务的完成数据，以及任务进度的走势，及时发现问题，对有拖延的任务进行有效针对性的管理。


## PRD

### （一）理论加值
***
> 如果说优秀的设计能让人们用最少的步骤达成目的，那么平静技术则让人们用最少的精力实现目的。

我们的前端产品使用了平静技术设计原则为产品本身进行了核心价值的加值。我们的平静技术主要涉及到了人的注意力等级。我们将注意力分为
1. 主要注意力 视觉相关
2. 次要注意力 无需关注就能感受到的如声音，震动等
3. 第三级注意力 注意力边缘的声音，光线或者环境震动等

####  平静技术1
app在**“个人中心-设置-消息提醒”**提供了在手机不同页面的消息提醒模式选择：
- 锁屏
- 手机首页
- 消息提醒页

而且还提供了手机提醒方式供用户选择：
- 声音
- 振动
- 呼吸灯

以上所有功能都提供了选择开启与否的选项。该功能在前端产品文档：链接
我们的**平静技术设计原则**体现在当用户进入手机主页，在进行如游戏等注意力需要高度集中的活动时，不会有消息弹窗影响到用户的活动也能提醒到用户。让用户能无压力了解到有消息后还能快速转回注意力到手中的活动。

####  平静技术2
app在**消息提醒-呼吸灯模式**中提供了不同亮色的呼吸灯提醒：

- 紧急事项消息：红色呼吸灯闪烁
- 系统消息提醒如任务消息：黄色呼吸灯闪烁
- 聊天互动消息提醒：白色呼吸灯闪烁

该功能在前端产品文档：链接
该功能**平静技术设计原则**体现在用户不需要去进行如点开屏幕，查看信息等繁杂操作来得知信息，我们产品不同颜色的闪烁灯能让用户以最少的精力获取足够多的信息量来决定是否要点开屏幕这个操作来进行下一步操作。

#### 平静技术3
app在**消息提醒-振动设置**中提供了振动功能模式：

- 短促振动一声：消息提醒
- 持久振动两秒：紧急事项消息

该**平静技术**功能考虑到了色盲人士或是无暇顾及手机的人。当用户将手机放在身侧或是口袋中无暇顾及，又想要实现不太过分散注意力而获取到想要的信息时，我们的设计利用无需关注就能感受到的振动变化来最大化给用户带来可用消息。

#### 平静技术4

app在**“任务指派-时间设置”**页面中提供了智能日历，让用户能够快速的从日历中指派任务时间段，还能在日历上迅速了解到日期的时间点信息如几号几号是星期几，免去了用户还得去翻看日历来安排任务的时间。让安排更加合理化。
![任务指派——时间设置.png](https://images.gitee.com/uploads/images/2020/0101/215111_16a14ee9_1648167.png)


#### 平静技术5

app在**“工作台-参与的项目列表”**提供了事项排序功能，排序可按照事项设定的重要程度进行索引也可以通过时间的迫近程度进行索引，让用户对自己的任务管理安排一目了然，不需要额外花心思去判断任务解决的前后顺序，让用户心中有数，安心完成任务。
![参与的项目列表.png](https://images.gitee.com/uploads/images/2020/0101/215111_a3937c8d_1648167.png)

### （二）前端产品信息设计
***
#### 1. 产品架构图

![产品架构图](https://images.gitee.com/uploads/images/2020/0102/003105_6086693c_1648167.png "产品架构图.png")

我们的产品功能架构如上，主要有“登录/注册”、“个人中心”、“联系人”、“模板库”、“工作台”、“历史创建”六个主要模块，每个模块下页面都已经如上图详细的列了出来。


在登录
#### 2. 主要功能使用流程图

![主要功能使用流程图](https://images.gitee.com/uploads/images/2020/0102/003142_5f48d074_1648167.png "主要功能使用流程图.png")

主要功能使用流程图中主要列有功能模块的运行逻辑，告知了用户具体的操作流程。
#### 3. 用户体系架构图

![用户体系架构图](https://images.gitee.com/uploads/images/2020/0101/201221_0a0e07cb_1648167.png "用户体系架构图.png")

### （三）前端产品与CMS的交互性
***
#### 交互1

工作台排布的是用户参与了的项目列表，在用户点击了单一项目后会转到用户所在项目被分配到的任务列表，在这里APP前端与CMS后端是实时交互的，用户在任务清单列表中进行了编辑任务并提交了的话，后台会立刻实时更新用户的任务列。如下图：

![前端任务列.png](https://images.gitee.com/uploads/images/2020/0101/215111_ef2c3321_1648167.png)

![CMS任务管理.png](https://images.gitee.com/uploads/images/2020/0101/215111_040e4a4a_1648167.png)

两张图分别对应的是前端任务列表和CMS的任务列表，两者会进行同步交互。

#### 交互2

产品的前端APP交流页面与CMS端的交流页面是实时交互的，无论用户在页面上传交互了什么都能同步更新。如下图：

![沟通.png](https://images.gitee.com/uploads/images/2020/0101/215111_ace87bef_1648167.png)

![即时通讯.png](https://images.gitee.com/uploads/images/2020/0101/215111_d4e36c3b_1648167.png)

#### 交互3

产品的前端APP在项目列表中设计了可视化的任务进度条，方便用户了解到项目的完成度，而这个功能在CMS后端任务详情中有交互对应的任务进度条，两者实现了同步更新，前端完成了任务，任务条变化后端也能同步变化。如下图：

![任务详情](https://images.gitee.com/uploads/images/2020/0102/004641_712d7ec0_1648167.png "任务详情.png")

![项目进度](https://images.gitee.com/uploads/images/2020/0102/004705_ecfc90bf_1648167.png "历史项目列表（含项目进度）.png")

### （四）前端产品原型
***

#### 主要功能阐述
- 登录页/注册页

![登录页.png](https://images.gitee.com/uploads/images/2020/0103/202318_63bb3efa_1648167.png)

用户可以在这个页面进行登录、注册、以及找回密码等操作，登录成功后将会进入主页面。

- 工作台

![参与的项目列表.png](https://images.gitee.com/uploads/images/2020/0103/202318_a19dffef_1648167.png)


工作台是app产品最主要的交互场所，工作台页面中罗列出了每个项目的信息以重要程度以及时间来排序，点击单个项目就会进入用户所在项目被分配的任务清单，用户可点击任务进入编辑页。

- 模板库

![模板库列表.png](https://images.gitee.com/uploads/images/2020/0103/202318_bd93ad7e_1648167.png)

用户可以在该页面进行新建项目或者是工作项目，也可以从下面提供的模板中挑选符合自己项目的模板，是本产品的功能亮点。



- [APP前端原型链接](http://lynn998.gitee.io/team_collaboration_app_frontend)